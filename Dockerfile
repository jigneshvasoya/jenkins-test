# This file is a template, and might need editing before it works on your project.
FROM python:3.6

RUN apt-get update \
    && apt-get install -y --no-install-recommends \
        python-pip \
    && rm -rf /var/lib/apt/lists/*

WORKDIR /usr/src/app

COPY requirements.txt /usr/src/app/
RUN pip install --no-cache-dir -r requirements.txt

COPY . /usr/src/app

# For some other command
# CMD ["python", "setup.py"]
